
import signal

import ServerBase

class ServerTemplate(ServerBase.ServerBase):
    def __init__(self, PVroot = 'MyServer', debug = False):
        self.version='1.0.0'
        self.program ='Server Template'
        super(ServerTemplate, self).__init__(PVroot,debug,'127.0.0.1', 5678) # last too numbers are the IP adress and port of watchdog

        # connect to the individual handler, which must have the function terminate() implemented
        # each handler should be their own thread to not interfere with the main process-loop
        self.handler={}

    def terminateSubThreads(self):
        for subserver in self.handler.keys():
            self.handler[subserver].terminate()

if __name__ == '__main__':

    debug = True
    server = ServerTemplate('SF-BC-SERVER', debug)
    signal.signal(signal.SIGTERM,server.terminate)
    try:
        server.run()
    except KeyboardInterrupt:
        server.terminate(None,None)
        




