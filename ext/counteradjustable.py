from slic.core.adjustable import Adjustable

class CounterAdjustable(Adjustable):
    def __init__(self, adjustable1, adjustable2):
        self.adj1=adjustable1
        self.adj2=adjustable2
        self.ref_values()     # implementation needs reference values to convert absolute scan to relative scan

    def ref_value(self):
        self.val1 = self.adj1.get_current_value(readback = False)
        self.val2 = self.adj2.get_current_value(readback = False)

    def set_target_value(self, value):
         t1 = self.adj1.set_target_value(self.val1 + value)
         t2 = self.adj2.set_target_value(self.val2 - value)
         t1.wait()
         t2.wait()

    def get_current_value(self):
         return self.adj1.get_current_value()

    def is_moving(self):
        return any([self.adj1.is_moving(),self.adj2.is_moving()])
