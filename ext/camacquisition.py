from time import sleep
from tqdm import trange
import h5py

from cam_server_client import PipelineClient
from cam_server_client.utils import get_host_port_from_stream_address
from bsread import source, SUB


from slic.core.acquisition.acquisition import Acquisition
class CamAcquisition(Acquisition):

    def getConnection(self,cam):
        pipeline_client = PipelineClient()
        cam_instance_name = str(cam) + "_sp1"
        stream_address = pipeline_client.get_instance_stream(cam_instance_name)
        self.host, self.port = get_host_port_from_stream_address(stream_address)
        print(self.host,self.port)

    def _acquire(self, filename, channels=None, data_base_dir=None, scan_info=None, n_pulses=100, **kwargs):
        print("my routine")
        print("extra kwargs:", kwargs)
        args = (filename, n_pulses, channels)
        args = ", ".join(repr(i) for i in args)
        print("acquire({})".format(args))
        print(f"dummy acquire to {filename}:")

#        stream_host,stream_port = getPipeLine(channels[0])
#        time.wait(1)
        data= []
        with source(host=self.host, port=self.port, mode=SUB) as input_stream:
            input_stream.connect()
            for i in range(n_pulses):
                print('Camera Images', i)
                message = input_stream.receive()
                data.append(message.data.data)
        hid = h5py.File(filename,'w')
        gid = hid.create_group(channels[0])
        for key in data[0].keys():
            gid.create_dataset(key, data = [rec[key].value for rec in data])
        hid.close()
