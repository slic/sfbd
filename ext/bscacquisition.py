
import h5py
import numpy as np

from slic.core.acquisition.acquisition import Acquisition


# class using the BSQueue to avoid to reestablish a stream for each step.

class BSCAcquisition(Acquisition):

    def setGroup(self,idx):
        self.grp = idx
        
    def _acquire(self, filename, channels=None, data_base_dir=None, scan_info=None, n_pulses=100, **kwargs):

        if not hasattr(self,'grp'):
            self.grp = 0
            
        queue =channels[0]  # abusing interface since BSAcquisition assume a list of channels
        # clear the queue
        queue.flush()
        # gather data
        data = [queue.__next__() for i in range(n_pulses)]
        # write out the data file
        hid = h5py.File(filename,'a')
        # save the pulse ID
        singledata = [ele['pid'] for ele in data]
        pidname = 'pulse_id/group%d' % self.grp
        hid.create_dataset(pidname, data = singledata)
        for chn in queue.channels:
            singledata = [ele[chn] for ele in data]
            if not chn == 'pid':
                dname = chn.replace(':','/')+'/data'
                hid.create_dataset(dname, data = singledata)
                dpid = dname.replace('/data','/pid')
                hid[dpid] = hid[pidname]
        hid.close()        
        

