from slic.core.adjustable import Adjustable,DummyAdjustable,PVAdjustable
import numpy as np

class PVCombiAdjustable(Adjustable):
    def __init__(self, adjustables,values):
        self.adjustables = adjustables
        self.values = values  # array or indiviual array values
       
               
    def set_target_value(self, value):
        print('##### Setvalue:',value)
        # check if values are in range
        for i,adj in enumerate(self.adjustables):
            idx = int(value[i])
            print('### Index',i, idx)
            val=self.values[i][idx]
            print('### Setting',adj.name,'to',val)
            adj.set_target_value(val)
       
    @property
    def units(self):
         return ['m' for adj in self.adjustables]        
#        return [adj.units() for adj in self.adjustables if adj is not None]

        
    def get_current_value(self, readback=True):
        return [adj.get_current_value() for adj in self.adjustables]  # this is different for dummy elements
   
    def stop(self):
        for adj in self.adjustbale:
            adj.stop()
            
    def is_moving(self):
        return any([adj.is_moving() for adj in self.adjustables])
            
   



        

        
