from slic.core.adjustable import PVAdjustable
from slic.utils import typename

class Magnet(PVAdjustable):

     def __init__(self,name):
          self.name=name
          pvsv='%s:I-SET' % name
          pvrb='%s:I-READ' % name
          tol = 0.075
          super().__init__(pvsv,pvname_readback=pvrb,accuracy=tol,internal=True)



     @property
     def status(self):
          return "Cycling"   

     def __repr__(self):
        tn = typename(self)
        return f"{tn} \"{self.name}\" is {self.status}"
