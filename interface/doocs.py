import subprocess


def doocsread(mystr):
    """ mystr input can be a string or a list """
    if isinstance(mystr, list):  # it's a list
        readCmd = ' '.join(mystr)
    else:
        readCmd = mystr
    cmd = ['doocsget', '-c', readCmd]
    MyOut = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout, stderr = MyOut.communicate()
    if b'result->error()' in stdout.split():
        print('ERROR: reading error')
        print(mystr)
        return
    result = [element.decode('utf-8') for element in stdout.split()]
    if len(result) == 1:  # one element only
        try:
            return float(result[0])
        except Exception as e:
            return result[0]
    else:
        try:
            return [float(x) for x in result]
        except Exception as e:
            return result


def doocswrite(mystr, target):
    cmd = ['doocsput', '-c', mystr, '-d', str(target)]
    MyOut = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout, stderr = MyOut.communicate()
