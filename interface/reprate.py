from time import sleep
from epics import PV
import numpy as np

class RepRate:
    def __init__(self, beamline = 'ARAMIS',debug=False):
        self.beamline = beamline
        self.debug=debug
        self.pv={}
        self.pv['BeamDelayAR'] = PV('SIN-TIMAST-TMA:Bunch-1-OnDelay-Sel')
        self.pv['BeamDelayAT'] = PV('SIN-TIMAST-TMA:Bunch-2-OnDelay-Sel')
        self.pv['RFDelay'] = PV('SIN-TIMAST-TMA:Beam-RF-OnDelay-Sel')
        self.pv['StopperAR'] = PV('SARMA02-MBNP100:REQUEST')
        self.pv['StopperAT'] = PV('SATMA01-MBNP100:REQUEST')
        self.pv['StopperAROut'] = PV('SARMA02-MBNP100:PLC_MOV_OUT')
        self.pv['StopperATOut'] = PV('SATMA01-MBNP100:PLC_NOV_OUT')
        self.pv['BeamRRAR'] = PV('SIN-TIMAST-TMA:Bunch-1-Freq-Sel')
        self.pv['BeamRRAT'] = PV('SIN-TIMAST-TMA:Bunch-2-Freq-Sel')
        self.pv['applyTiming'] = PV('SIN-TIMAST-TMA:Beam-Apply-Cmd.PROC')
        self.rrReal = np.array([100., 50., 33.3, 25., 16.6, 12.5, 10., 8.3, 5., 2.5, 1.])
        self.rrId = np.arange(0, 11)

        
    def setStopper(self,doinsert):
        if self.beamline == 'ARAMIS':
            pv = self.pv['StopperAR']
        else:
            pv = self.pv['StopperAT']
        if self.debug:
            print('DEBUG: Setting beam stopper to:',doinsert)
            return
        if doinsert:
            pv.value = 1
        else:
            pv.value = 0

    def isStopperOpen(self):
        if self.beamline == 'ARAMIS':
            return self.pv['StopperAROut']
        else:
            return self.pv['StopperATOut']


    def stopSF(self):
        if self.debug:
            print('DEBUG: Stopping Machine disabled')
            return
        if self.beamline == 'ARAMIS':
            self.pv['BeamDelayAR'].value = 1
        self.pv['BeamDelayAT'].value = 1
        sleep(0.1)
        self.pv['RFDelay'].value =1 
        sleep(0.1)
        self.pv['applyTiming'].value = 1
        
    def getRR(self):
        if self.beamline == 'ARAMIS':
            return self.rrReal[self.pv['BeamRRAR'].value]
        else:
            return self.rrReal[self.pv['BeamRRAT'].value]
            

    def setRR(self,rr=1.):
        """
        Possible RR as of 14/01/2020
        0 100.00 Hz
        1 50.00 Hz
        2 33.33 Hz
        3 25.00 Hz
        4 16.66 Hz
        5 12.50 Hz
        6 10.00 Hz
        7 8.33 Hz
        8 5.00 Hz
        9 2.50 Hz
        10 1.00 Hz
        11 Bunch Off
        """

        findRR = abs(self.rrReal - rr) < 0.1
        if findRR.sum():  # at least one rr is matching
            rrSel = self.rrId[np.argmax(findRR)]
        else:
            print('Requested rep. rate %.3f is not available' % rr)
            return

        if self.debug:
            print('DEBUG: Setting beam rate to %d (%f Hz)' % (rrSel,rr))
            return
            

        if self.beamline == 'ARAMIS':
            self.pv['BeamRRAR'].value = rrSel
            sleep(0.1)
            self.pv['BeamDelayAR'].value = 0  # 0 on beam, 1 on delay
            rrAT = self.pv['BeamRRAT'].value
            if rrAT < rrSel:    # reduce athos at least to same rep rate as Aramis
                self.pv['BeamRRAT'].value = rrSel
                sleep(0.1)
                self.pv['BeamDelayAT'].value = 0  # 0 on beam, 1 on delay
        else:
            self.pv['BeamRRAT'].value = rrSel
            sleep(0.1)
            self.pv['BeamDelayAT'].value = 0  # 0 on beam, 1 on delay

        sleep(0.1)
        if rrSel == 11:
            self.pv['RFDelay'].value =1
        else:
            self.pv['RFDelay'].value = 0
        sleep(0.1)
        self.pv['applyTiming'].value = 1
