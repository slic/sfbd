import numpy as np

from bstrd import BSCache

class SimpleCapture:
    def __init__(self):
        self.chn=None
        self.bs = BSCache(100000,100000)   # 1000 second time out, capazity for 1000 second.
        self.abort=False

    def terminate(self):
        self.bs.stop()
        self.bs.pt.running.clear()
        
    def stop(self):
        self.abort=True
        
    def initChannels(self,channels):
        self.chn=channels
        self.bs.get_vars(channels)

    def acquire(self,N=1,reprate=-1,stop=False):
        self.abort=False
        data = np.zeros((N,len(self.chn)))
        pid = np.zeros((N))
        self.bs.flush()
        idx = 0
        while idx<N and not self.abort:
            rec=self.bs.__next__()
            pid[idx] = rec['pid']
            data[idx,:] = np.array([rec[ch] for ch in self.chn])
            idx += 1
        ret={}
        for i,ch in enumerate(self.chn):
            ret[ch]=data[:,i]
        ret['pid'] = pid

        return ret
