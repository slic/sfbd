import sys
import signal
import os
import socket
import logging
import logging.handlers
from logging.handlers import RotatingFileHandler
from datetime import datetime
import time

from epics import PV
sys.path.append('/sf/bd/packages/sfbd')
from sfbd.util import ZMQBase

class ServerBase(ZMQBase):
    def __init__(self, root = 'MyServer', debug = False, WDServer = '127.0.0.1', WDPort = 5678):

        super(ServerBase,self).__init__(WDServer,WDPort)

        self.debug = debug
        self.root = root
        self.suffix=''
        if self.debug:
            self.suffix='-SIMU'
        self.host = socket.gethostname()
        self.pid = os.getpid()  # process ID
       
        # enabling logging
        self.logfilename="/sf/data/applications/BD-SERVER/%s.log" % self.root
        handler = RotatingFileHandler(filename=self.logfilename,
                                      mode='a',
                                      maxBytes=5 * 1024 * 1024,
                                      backupCount=1,
                                      delay=0)
        if self.debug:
            logging.basicConfig(level=logging.INFO,
                                format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                                datefmt='%m-%d %H:%M:%S')
        else:
            logging.basicConfig(level=logging.INFO,
                                format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                                datefmt='%m-%d %H:%M:%S',
                                handlers=[handler,])
        self.logger=logging.getLogger(self.program)

        # setting up ZMQ interface
        self.ZMQServerInfo(self.root,self.host,self.pid)       

        # individual channels of main thread
        self.PVstop = PV('%s:STOP%s' % (self.root,self.suffix))
        self.PVstop.value = 0
        self.PVstop.add_callback(self.stop)
        self.PVping = PV('%s:PING%s' % (self.root,self.suffix))
        self.PVlog =  PV('%s:LOG%s' % (self.root,self.suffix))

    def stop(self,pvname=None,value=None,**kws):
        if value > 0:
            self.logger.info('PV:STOP triggered at %s' % datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            self.running=False

    def start(self):
        self.logger.info('Starting Server: %s at %s' % (self.root,datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        self.logger.info('PV Root: %s' % self.root)
        self.logger.info('Version: %s' % self.version)
        self.logger.info('Host: %s' % self.host)
        self.logger.info('PID: %d' % self.pid)
        if self.debug:
            self.logger.info('Debug Mode')
 
    def run(self):
        self.start()
        self.running=True
        while self.running:
            time.sleep(1)
            if self.ZMQPoll():
                self.logger.info('Watchdog Server requested termination')
                self.running = False
            self.PVping.value = datetime.now().strftime('Last active at %Y-%m-%d %H:%M:%S')
        self.terminate(None,None)

    def terminate(self,signum,frame):
        # stopping any sub thread with the server specific function terminateSubThreads
        self.terminateSubThreads()
        self.logger.info('Terminating Server at %s' % datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self.ZMQPoll('quit')  # informing the watchdog
        print('Bunch Compressor Server is quitting...')
        sys.exit(0)





