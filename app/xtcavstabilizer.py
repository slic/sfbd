import time
import numpy as np

from bstrd import BSCache
from epics import PV


class XTCAVStabilizer:
    """
    Wrapper class to bundle all daq/io needed for stabilizing the XTCAV
    """
    def __init__(self):

        # the PV
        self.PVTCAV = PV('SATMA02-RMSM:SM-GET',connection_timeout=0.9, callback=self.stateChanged)
        self.state=self.PVTCAV.value == 9
        self.PVVolt  = PV('SATMA02-RSYS:SET-ACC-VOLT')
        self.PVPhase = PV('SATMA02-RSYS:SET-BEAM-PHASE')
        
        # the BS channels
        self.bs = BSCache(100000,10000)  # 100 second timeout, size for 100 second data taken         
        self.channels = ['SATBD02-DBPM040:X2','SATMA02-RLLE-DSP:PHASE-VS','SATBD02-DBPM040:X2-VALID']
        self.validation = self.channels[2]
        self.bs.get_vars(self.channels)  # this starts the stream into the cache
        self.bs.stop()

    def getPhase(self):
        return self.PVPhase.value

    def setPhase(self,phi):
        self.PVPhase.set(phi)
    
    def getVoltage(self):
        return self.PVVolt.value

    def stateChanged(self,pvname=None, value=0, **kws):
        self.state = value == 9
         
    def terminate(self):
        print('Stopping BSStream Thread...')    
        self.bs.stop()
        self.bs.pt.running.clear() # for some reason I have to

    def flush(self):
        self.bs.flush()

    def read(self):
        return self.bs.__next__()


        
