from bstrd import BSCache
from epics import PV

athos_uncalibrated = 'SATFE10-PEPG046-EVR0:CALCI'
athos_calibrated = 'SATFE10-PEPG046:FCUP-INTENSITY-CAL'
aramis_uncalibrated = 'SARFE10-PBIG050-EVR0:CALCI'
aramis_calibrated = 'SARFE10-PBPG050:HAMP-INTENSITY-CAL'
eehg_spectrometer = 'SSL2-CPCW-SPEC01:SPECTRUM'
channel_dict = {
        'PSSS': ['SARFE10-PSSS059:SPECTRUM_Y'],
        'PMOS Maloja': ['SATOP21-PMOS127-2D:SPECTRUM_Y', athos_uncalibrated, athos_calibrated],
        'PMOS Furka': ['SATOP31-PMOS132-2D:SPECTRUM_Y', athos_uncalibrated, athos_calibrated],
        'PSSS LB': ['SARFE10-PSSS059-LB:SPECTRUM_Y'],
        'PSSS incl gasd': ['SARFE10-PSSS059:SPECTRUM_Y', aramis_uncalibrated, aramis_calibrated],
        'PSSS LB incl gasd': ['SARFE10-PSSS059-LB:SPECTRUM_Y', aramis_uncalibrated, aramis_calibrated],
        'PMOS Maloja EEHG': ['SATOP21-PMOS127-2D:SPECTRUM_Y', eehg_spectrometer],
#        'PMOS Maloja EEHG': ['SARFE10-PSSS059:SPECTRUM_Y', eehg_spectrometer],
        'PMOS Furka EEHG': ['SATOP21-PMOS127-2D:SPECTRUM_Y', eehg_spectrometer],
        }
names = ['PSSS', 'PMOS Maloja', 'PMOS Furka', 'PSSS LB', 'PSSS incl gasd', 'PSSS LB incl gasd', 'PMOS Maloja EEHG','PMOS Furka EEHG' ]

class SpectralAnalysis:
    def __init__(self):
        self.bs = BSCache(100000,10000)  # 100 second timeout, size for 100 second data taken
        self.bs.stop()
        self.channel = None
        self.channels = [channel_dict[x] for x in names]
        self.hasBStream=False
       
    def connect_name(self, name):
        index = names.index(name)
        self.connect(index)

    def connect(self,ich):
        if ich < 0 or ich >= len(self.channels):
            return False
        self.channel = self.channels[ich]
        print('Connecting to BS-Channel:',self.channel)
        self.bs.channels.clear()
        self.hasBStream=True
        try:
            self.bs.get_vars(self.channel)  # this starts the stream into the cache
        except ValueError:
            print('Cannot find requested channels in BS stream')
            self.hasBStream=False
        self.pv = PV(self.channel[0].replace('_Y','_X'))

    def terminate(self):
        print('Stopping BSStream Thread...')
        self.bs.stop()
        self.bs.pt.running.clear() # for some reason I have to
        self.pv.disconnect()

    def flush(self):
        self.bs.flush()

    def read(self):
        return next(self.bs)

    def read_spectrum_axis(self):
        return self.pv.value

    def getSpectrometerName(self):
        return self.channel

class SpectralAnalysis2:
    def __init__(self):
        self.bs1 = BSCache(100000, 10000)
        self.bs2 = BSCache(100000, 10000)
        self.bs1.stop()
        self.bs2.stop()
        self.hasBStream=False

    def connect_name(self, name):
        channels = channel_dict[name]
        self.bs1.channels.clear()
        self.hasBStream = True
        try:
            self.bs1.get_vars(channels[:1])  # this starts the stream into the cache
        except ValueError:
            print('Cannot find requested channel %s in BS stream' % channels[0])
            self.hasBStream=False
        if len(channels) > 1:
            try:
                self.bs2.get_vars(channels[1:])  # this starts the stream into the cache
            except ValueError:
                print('Cannot find requested channel %s in BS stream' % channels[1:])
                self.hasBStream=False
        self.pv = PV(channels[0].replace('_Y','_X'))

    def read_spectrum_axis(self):
        return self.pv.value

    def flush(self):
        for _bs in self.bs1, self.bs2:
            _bs.flush()

    def read(self):
        return next(self.bs1), next(self.bs2)

    def terminate(self):
        print('Stopping BSStream Thread...')
        for _bs in self.bs1, self.bs2:
            _bs.stop()
            _bs.pt.running.clear() # for some reason I have to
        self.pv.disconnect()

