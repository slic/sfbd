import datetime
import numpy as np

from slic.core.acquisition import PVAcquisition
from slic.core.acquisition import BSAcquisition
from slic.core.adjustable import PVAdjustable
from slic.devices.general import motor
from slic.core.scanner import Scanner
from sfbd.ext import CamAcquisition

# some temporary wrapper
class PollingPVAcquisition(PVAcquisition):
     def _acquire(self, *args, polling=True, **kwargs):
        return super()._acquire(*args, polling=polling, **kwargs)

class LaserScanBase:
    def __init__(self):
        print('Init Base Class')
        self.SV= 'SSL-LMOT-M1104:MOT'
        self.pol = motor.Motor(self.SV)
#        self.pol = PVAdjustable(self.SV)

    def stop(self):
        if self.sc is None:
            return
        self.sc.stop()

    def running(self):
        return self.sc.running

    def status(self):
        si = self.sc.scan_info.to_dict()
        steps = 0
        if 'scan_values' in si:
            steps=len(si['scan_values'])
        return steps,self.N

    def info(self):
        return self.sc.scan_info.to_dict()

    def setup(self,amax=45,Nsteps=5,Nsamples=5):
        amin = 0
        self.N = Nsteps
        self.Ns= Nsamples
        amin = 15
        amax = 22
        self.values=np.linspace(amin,amax,num=self.N)  # needs a change

# measuring the pulse energy as a function of the controling PV. Note that the power should be limited to 300 uJ
# thus limiting the value of the actuaor defining the lase rpulse energy in the EnergyModulaiton class.

class LaserPower(LaserScanBase):
    def __init__(self):
        super(LaserPower,self).__init__()

        self.scanname = 'HEROLaserEnergy'
        dirpath= datetime.datetime.now().strftime('/sf/data/measurements/%Y/%m/%d/slic_sfbd')
        self.scandir='%s/%s' % (dirpath,self.scanname)

        self.RB = 'SSL-LENG-SLNK1:VAL_GET'
        self.erg = PVAcquisition("machine","sfbd", default_channels=[self.RB])
     
        self.scanner = Scanner(data_base_dir=self.scandir,scan_info_dir=self.scandir,make_scan_sub_dir=True,
                               default_acquisitions=[self.erg])
        
    def scan(self):
        self.sc=self.scanner.ascan_list(self.pol,self.values,
                                    filename=self.scanname,start_immediately = False,
                                    n_pulses=self.Ns,return_to_initial_values=True)
        self.sc.run()
        



# measuring the coherent emission/space charge blow-up as a function of the hero energy modulation

class EnergyModulation(LaserScanBase):
    def __init__(self, acq = 0):
        super(EnergyModulation,self).__init__()
        self.scanname = 'HEROEnergyModulation'
        dirpath= datetime.datetime.now().strftime('/sf/data/measurements/%Y/%m/%d/slic_sfbd')
        self.scandir='%s/%s' % (dirpath,self.scanname)

        self.acq = acq
        if self.acq == 0:
             self.RB ='SATFE10-PEPG046-EVR0:CALCI' 
             self.erg = BSAcquisition("machine","sfbd", default_channels=[self.RB])
        elif self.acq == 1:
             self.RB ='SATBD02-DBPM040:Y2'
             self.erg = BSAcquisition("machine","sfbd", default_channels=[self.RB])
        elif self.acq == 2:
             self.RB = 'SATBD01-DSCR210'
             self.erg = CamAcquisition("machine","sfbd", default_channels=[self.RB])
             self.erg.getConnection(self.RB)
        else:
             self.RB = 'SATBD02-DSCR050'
             self.erg = CamAcquisition("machine","sfbd", default_channels=[self.RB])
             self.erg.getConnection(self.RB)
        
        self.scanner = Scanner(data_base_dir=self.scandir,scan_info_dir=self.scandir,make_scan_sub_dir=True,
                               default_acquisitions=[self.erg])
        
    def scan(self):
        self.sc=self.scanner.ascan_list(self.pol,self.values,
                                        filename=self.scanname,start_immediately = False,
                                        n_pulses=self.Ns,return_to_initial_values=True)
        self.sc.run()
        




 

  
